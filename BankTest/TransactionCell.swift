//
//  TransactionCell.swift
//  BankTest
//
//  Created by Eugene Fozekosh on 8/16/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {
    
    @IBOutlet var transactionNameLabel: UILabel!
    @IBOutlet var transactionDateLabel: UILabel!
    @IBOutlet var transactionIconButton: UIButton!
    @IBOutlet var transactionTotalLabel: UILabel!

    override func awakeFromNib() {
        
        transactionIconButton.layer.cornerRadius = transactionIconButton.frame.size.width/2.0
        transactionIconButton.layer.borderWidth = 1.0
        transactionIconButton.layer.borderColor = UIColor(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0).cgColor
    }
    
    func updateFromModel(model: Transaction) -> Void {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dateString = formatter.string(from: model.date!)

        transactionDateLabel.text = dateString
        transactionNameLabel.text = model.title
        transactionTotalLabel.text = model.amount?.stringValue
        transactionTotalLabel.textColor = (model.title?.contains("Покупка"))! ? UIColor.init(red: 78.0/255.0, green:164.0/255.0, blue: 29.0/255.0, alpha: 1.0) : UIColor.init(red: 223.0/255.0, green: 41.0/255.0, blue: 41.0/255.0, alpha: 1.0)

        // I would propose to send some bool value in Transaction model instead of this ugly logic
        let image = (model.title?.contains("Покупка"))! ? UIImage(named: "ic_income") : UIImage(named: "ic_outcome")
        transactionIconButton.setImage(image, for: UIControlState.normal)
        
    }
}
