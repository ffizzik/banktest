//
//  NetworkManager.swift
//  BankTest
//
//  Created by Eugene Fozekosh on 8/10/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager{
    
    struct API {
        static let kHost = "http://test-ios.w-m.ru/api/v1"
        
    }
    public static let sharedInstance = NetworkManager()
    var date: Date?
    typealias CompletionHandler = (_ dataArray: Array<Portfolio>?, _ error: Error?) -> Void
    typealias TransactionsHandler = (_ dataArray: Array<Transaction>?, _ error: Error?) -> Void
    
    func getDateWith(completion: @escaping (Date?) -> Void) {
        
        let url = API.kHost + "/date"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse) in
            
            switch response.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let dateFor: DateFormatter = DateFormatter()
                dateFor.dateFormat = "yyyy-MM-dd"
                let finalDate: Date? = dateFor.date(from: JSON.value(forKey: "max_date") as! String)
                NetworkManager.sharedInstance.date = finalDate
                completion(finalDate! as Date)
                break
            case .failure(let error):
                completion(nil)
                debugPrint(error as Any)
            }
        }
    }
    
    func getPortfolioValueWith(currency: String, completion: @escaping (String?) -> Void) {
        NetworkManager.sharedInstance.getDateWith { (dateString) in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            
            let myStringafd = formatter.string(from: NetworkManager.sharedInstance.date!)
            
            let url = API.kHost + "/portfolio-value"
            
            let params: Parameters = [ "date": myStringafd,
                                       "currency": currency]
            
            Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse) in
                
                switch response.result {
                case .success(let value):
                    let JSON = value as! NSDictionary
                    completion(JSON.value(forKey: "amount") as? String)
                    break
                case .failure(let error):
                    completion(nil)
                    debugPrint(error as Any)
                }
            }
        }
    }
    
    func getPortfolioTransactionsWith(portfolioID: String, completion: @escaping (TransactionsHandler)) {
        let url = API.kHost + "/fund-operations"
        
        let params: Parameters = ["fund_id": portfolioID]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse) in
            var result:Array<Transaction> = []
            
            switch response.result {
            case .success(let value):
                guard let JSON = value as? Array<Dictionary<String, Any>> else {
                    completion(nil, nil)
                    return
                }
                
                for model in JSON{
                    let transaction: Transaction = Transaction()
                    transaction.updateFromDictionary(dict: model)
                    result.append(transaction)
                }
                break
            case .failure(let error):
                completion(nil, nil)
                debugPrint(error as Any)
            }
            completion(result, nil)
        }
    }
    
    func getDisclaimerTextForIDs(portfolioIDs: String, completion: @escaping (String?) -> Void) {
        
        let url = API.kHost + "/disclaimers"
        let params: Parameters = ["fund_ids": portfolioIDs]
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse) in
            
            var resultString = ""
            
            switch response.result {
            case .success(let value):
                guard let JSON = value as? Array<Dictionary<String, Any>> else {
                    completion(nil)
                    return
                }
                for model in JSON{
                    resultString += "\(model["text"] as! String) \n\n"
                }
                
                completion(resultString)
                break
            case .failure(let error):
                completion(nil)
                debugPrint(error as Any)
            }
        }
    }
    
    
    func getPortfoliosWith(completion: @escaping (CompletionHandler)) {
        NetworkManager.sharedInstance.getDateWith { (dateString) in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            
            let myStringafd = formatter.string(from: NetworkManager.sharedInstance.date!)
            
            let url = API.kHost + "/portfolio"
            
            let params: Parameters = [ "date": myStringafd]
            
            Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse) in
                
                var result:Array<Portfolio> = []
                
                
                switch response.result {
                case .success(let value):
                    let JSON = value as! NSDictionary
                    let funds = JSON["funds"] as! Array<Any>
                    for model in funds{
                        let fund: Portfolio = Portfolio()
                        fund.updateFromDictionary(dict: model as! Dictionary<String, Any>)
                        result.append(fund)
                    }
                    break
                case .failure(let error):
                    completion(nil, nil)
                    debugPrint(error as Any)
                }
                completion(result, nil)
            }
        }
    }
    
    
    func getPortfolioAmountWith(id: NSNumber, completion: @escaping (String?) -> Void) {
        NetworkManager.sharedInstance.getDateWith { (dateString) in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            
            let myStringafd = formatter.string(from: NetworkManager.sharedInstance.date!)
            
            let url = API.kHost + "/fund-amount"
            
            let params: Parameters = [ "date": myStringafd,
                                       "fund_id": id]
            
            Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse) in
                
                switch response.result {
                case .success(let value):
                    let JSON = value as! NSDictionary
                    completion(JSON.value(forKey: "amount") as? String)
                    break
                case .failure(let error):
                    completion(nil)
                    debugPrint(error as Any)
                }
            }
        }
    }
}


class Transaction: NSObject {
    var title : String?
    var date : Date?
    var amount : NSNumber?
    var currency : String?
    
    func updateFromDictionary(dict: Dictionary<String, Any>) -> Void {
        
        let valueDict: Dictionary<String,Any> = (dict["value"] as? Dictionary)!
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        self.date = formatter.date(from: (dict["date"] as? String)!)
        self.title = dict["title"] as? String
        self.amount = valueDict["amount"] as? NSNumber
        self.currency = valueDict["currency"] as? String
    }
}



class Portfolio: NSObject {
    var id : NSNumber?
    var name : String?
    var link : String?
    
    func updateFromDictionary(dict: Dictionary<String, Any>) -> Void {
        self.id = dict["id"] as? NSNumber
        self.name = dict["name"] as? String
        self.link = dict["link"] as? String
    }
}

