//
//  FundCell.swift
//  BankTest
//
//  Created by Eugene Fozekosh on 8/10/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

import UIKit
import MBProgressHUD

class FundCell: UITableViewCell {

    @IBOutlet var fundNameLabel: UILabel!
    @IBOutlet var fundAmountLabel: UILabel!

    func updateFromModel(model: Portfolio) -> Void {
        let progress = MBProgressHUD.showAdded(to: self.contentView, animated: true)
        progress?.color = UIColor.clear

        NetworkManager.sharedInstance.getPortfolioAmountWith(id: model.id!) { (amount) in
            progress?.hide(true)
            self.fundNameLabel.text = model.name
            self.fundAmountLabel.text = "\(String(describing: amount!)) ₽"
        }
    }

    
}
