//
//  InfoCell.swift
//  BankTest
//
//  Created by Eugene Fozekosh on 8/16/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

import UIKit

class InfoCell: UITableViewCell {
    
    @IBOutlet var infoTextView: UITextView!

}
