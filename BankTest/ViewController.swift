//
//  ViewController.swift
//  BankTest
//
//  Created by Eugene Fozekosh on 8/10/18.
//  Copyright © 2018 Eugene Fozekosh. All rights reserved.
//

import UIKit
import MBProgressHUD

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var fundsTableView: UITableView!
    @IBOutlet var transactionsTableView: UITableView!
    @IBOutlet var lastOperationsTableView: UITableView!
    @IBOutlet var gradientView: UIView!
    
    var portfolios: Array<Portfolio> = []
    var transactions: Array<Transaction> = []
    var portfolioIDs: String = ""
    var progress: MBProgressHUD = MBProgressHUD()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureGradientView()
        NetworkManager.sharedInstance.getPortfolioValueWith( currency: "RUB") { (value) in
            self.totalLabel.text = "\(String(describing: value!)) ₽"
        }
        
        NetworkManager.sharedInstance.getPortfoliosWith() { (array, error) in
            guard error == nil else {
                return
            }
            self.portfolios = array!
            self.portfolioIDs = (self.portfolios.map{$0.id!.stringValue}).map{String($0)}.joined(separator: ",")
            self.fetchTransactions(withPortfolios: self.portfolios)
            self.fundsTableView.reloadData()
        }
    }
    
    func fetchDisclaimer(withIDs: String){
        NetworkManager.sharedInstance.getDisclaimerTextForIDs(portfolioIDs: withIDs) { (result) in
            self.transactions = self.transactions.sorted(by: { $0.date! > $1.date!})
            self.transactions = Array(self.transactions[0..<5])
            self.progress.hide(true)
            
            self.transactionsTableView.reloadData()
        }
    }
    
    func fetchTransactions(withPortfolios: Array<Portfolio>){
        self.progress = MBProgressHUD.showAdded(to: self.transactionsTableView, animated: true)
        self.progress.color = UIColor.clear
        let group = DispatchGroup()
        for portfolio in withPortfolios {
            print("\(String(describing: portfolio.name)) started")
            
            group.enter()
            NetworkManager.sharedInstance.getPortfolioTransactionsWith(portfolioID: (portfolio.id?.stringValue)!) { (transactions, error) in
                print("\(String(describing: portfolio.name)) finished")
                self.transactions += transactions!
                group.leave()
            }
        }
        
        group.notify(queue: .main) {
            self.fetchDisclaimer(withIDs: self.portfolioIDs)
        }
    }
    
    func configureGradientView() {
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [UIColor(red: 28.0/255.0, green: 30.0/255.0, blue: 93.0/255.0, alpha: 1.0).cgColor, UIColor(red: 96.0/255.0, green: 40.0/255.0, blue: 90.0/255.0, alpha: 1.0).cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.gradientView.frame.size.width, height: self.gradientView.frame.size.height)
        
        self.gradientView.layer.insertSublayer(gradient, at: 0)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == transactionsTableView {
            return 59.0
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == transactionsTableView {
            let width = tableView.frame.width
            
            let label = UILabel(frame: CGRect(x: 20, y: 0, width: width, height: 59.0))
            label.font = UIFont (name: "Roboto-Regular", size: 12)
            label.text = "ПОСЛЕДНИЕ ОПЕРАЦИИ"
            label.textColor = UIColor(red: 149.0 / 255.0, green: 149.0 / 255.0, blue: 149.0 / 255.0, alpha: 1)
            
            let header = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 59.0))
            header.backgroundColor = UIColor.white
            header.addSubview(label)
            
            return header
        }
        else{
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == fundsTableView {
            return portfolios.count
        }else if(tableView == transactionsTableView){
            return transactions.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == fundsTableView {
            let cell: FundCell = tableView.dequeueReusableCell(withIdentifier: "fundCell") as! FundCell
            
            cell.updateFromModel(model: portfolios[indexPath.row])
            
            return cell
        }else{
            let cell: TransactionCell = tableView.dequeueReusableCell(withIdentifier: "transactionCell") as! TransactionCell
            cell.updateFromModel(model: transactions[indexPath.row])
            
            return cell
        }
    }

}


